<?php

namespace WebsiteControlPanel\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use WebsiteControlPanel\BackendBundle\Entity\View;
use WebsiteControlPanel\BackendBundle\Entity\ContactoCotizar;
use WebsiteControlPanel\BackendBundle\Entity\Testimonio;
use WebsiteControlPanel\BackendBundle\Entity\ContactMail;
use WebsiteControlPanel\BackendBundle\Form\ContactoCotizarType;
use WebsiteControlPanel\BackendBundle\Form\TestimonioType;
use WebsiteControlPanel\BackendBundle\Form\ContactMailType;
use WebsiteControlPanel\BackendBundle\Util\Util;

/**
 * @author Juan José Molina <jmolina@kijho.com>
 */
class DefaultController extends Controller {

    /**
     * index de la pagina.
     * @return type
     */
    public function indexAction() {

        $em = $this->getDoctrine()->getManager();
        //las 6 primeras imagenes en la galeria y ordenadas por el sortable

        $gallery = $em->getRepository('BackendBundle:GalleryImages')->findBy(
                array(), array('order' => 'ASC'), 6, 0);
        $testimonio = $em->getRepository('BackendBundle:Testimonio')->findBy(
                array(), array('order' => 'ASC'), 6, 0);
        $request = $this->getRequest();
        $pageJump = $request->query->get('pageJump');
        $entity = new ContactMail;
        $formMail = $this->createForm(new ContactMailType(), $entity);

        return $this->render('FrontendBundle:Default:index.html.twig', array(
                    'pageJump' => $pageJump,
                    'formMail' => $formMail->createView(),
                    'galeria' => $gallery,
                    'testimonios' => $testimonio,
                    'page' => 'index'
        ));
    }

    public function fancyRazasAction($id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Raza')->find($id);
        $slider = $em->getRepository('BackendBundle:ImageSliderRaza')->findOneBy(array('raza' => $id));
        $entityCotizar = new ContactoCotizar;
        $form = $this->createForm(new ContactoCotizarType(), $entityCotizar);

        return $this->render('FrontendBundle:Default:fancyRazas.html.twig', array(
                    'entity' => $entity,
                    'slider' => $slider,
                    'entityCotizar' => $entityCotizar,
                    'form' => $form->createView(),
        ));
    }

    public function fancyTestiAction() {

        $entity = new Testimonio;
        $form = $this->createForm(new TestimonioType(), $entity);

        return $this->render('FrontendBundle:Default:fancyTestimonio.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    public function fancyRazasAddAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Raza')->find($id);
        $entityCotizar = new ContactoCotizar;
        $form = $this->createForm(new ContactoCotizarType(), $entityCotizar);
        $form->handleRequest($request);
        $salto = false;
        $slider = $em->getRepository('BackendBundle:ImageSliderRaza')->findOneBy(array('raza' => $id));

        if ($form->isValid()) {
            $entityCotizar->setRaza($entity);
            $entityCotizar->setEstado(false);
            $em->persist($entityCotizar);
            $em->flush();
            $salto = true;
        } else {
            $salto = false;
        }

        return $this->render('FrontendBundle:Default:fancyRazas.html.twig', array(
                    'salto' => $salto,
                    'slider' => $slider,
                    'entity' => $entity,
                    'entityCotizar' => $entityCotizar,
                    'form' => $form->createView(),
        ));
    }

    public function fancyTestimonioAddAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $entity = new Testimonio;
        $form = $this->createForm(new TestimonioType(), $entity);
        $form->handleRequest($request);
        $salto = false;
//        \Symfony\Component\VarDumper\VarDumper::dump($form->getErrors());die();
        if ($form->isValid()) {
            $entity->setEstado(0);
            $em->persist($entity);
            $em->flush();
            $salto = true;
        }

        return $this->render('FrontendBundle:Default:fancyTestimonio.html.twig', array(
                    'salto' => $salto,
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }
    
    public function fancyCondicionesViewAction() {   

        return $this->render('FrontendBundle:Default:fancyCondiciones.html.twig', array(
                 ));
    }

    public function fullGalleryAction() {

        $em = $this->getDoctrine()->getManager();
        $gallery = $em->getRepository('BackendBundle:GalleryImages')->findAll();
        $entity = new ContactMail;
        $formMail = $this->createForm(new ContactMailType(), $entity);

        return $this->render('FrontendBundle:Default:fullGallery.html.twig', array(
                    'galeria' => $gallery,
                    'formMail' => $formMail->createView(),
        ));
    }

    public function nosotrosAction() {

        $entity = new ContactMail;
        $formMail = $this->createForm(new ContactMailType(), $entity);

        return $this->render('FrontendBundle:Default:nosotros.html.twig', array(
                    'formMail' => $formMail->createView(),
        ));
    }

    /**
     * metodo para contar las visitas en la pagina
     * @author Juan José Molina <jmolina@kijho.com>
     * @since 1.0 08/11/2015
     * @return \WebsiteControlPanel\FrontendBundle\Controller\Response
     */
    public function newPageViewAction() {

        $em = $this->getDoctrine()->getManager();
        $view = new View();
        $views = $em->getRepository('BackendBundle:View')->findAll();
        $total = count($views) + 1;
        $view->setTotal($total);
        $em->persist($view);
        $em->flush();
        $response['result'] = '__OK__';
        $r = new Response(json_encode($response));
        $r->headers->set('Content-Type', 'application/json');

        return $r;
    }

    /**
     * metodo para generar las vistas de cotizar razas con sus respectivos tipos
     * @author Juan José Molina <jmolina@kijho.com>
     * @since 1.0 08/11/2015
     * @return \WebsiteControlPanel\FrontendBundle\Controller\Response
     */
    public function cotizarRazasAction() {

        $em = $this->getDoctrine()->getManager();
        $razasPequeñas = $em->getRepository('BackendBundle:Raza')->findBy(array('tipo' => 0));
        $razasMedianas = $em->getRepository('BackendBundle:Raza')->findBy(array('tipo' => 1));
        $razasGrandes = $em->getRepository('BackendBundle:Raza')->findBy(array('tipo' => 2));
        $entity = new ContactMail;
        $formMail = $this->createForm(new ContactMailType(), $entity);

        return $this->render('FrontendBundle:Default:cotizarRazas.html.twig', array(
                    'razasPequeñas' => $razasPequeñas,
                    'razasMedianas' => $razasMedianas,
                    'razasGrandes' => $razasGrandes,
                    'formMail' => $formMail->createView(),
        ));
    }

    /**
     * Metodo para abrir fancy y de esta foram enviar correo adjuntando hoja de vida
     * @param Request $request
     * @return type
     */
    public function contactAction() {

        $em = $this->getDoctrine()->getManager();

        //Formulario de Worker
        $contact = new ContactMail();
        $formMail = $this->createForm(new ContactMailType, $contact);
        $request = $this->getRequest();
        $gallery = $em->getRepository('BackendBundle:GalleryImages')->findBy(
                array(), array('order' => 'ASC'), 6, 0);
        $testimonio = $em->getRepository('BackendBundle:Testimonio')->findBy(
                array(), array('order' => 'ASC'), 6, 0);        
        $pageJump = $request->query->get('pageJump');  
        $formMail->handleRequest($request);
            //Validación de la información enviada en el formulario
            if ($formMail->isValid()) {

                //Asignación fecha-hora de envio correo y guardado en base de datos               
                $contact->setCreationDate(Util::getCurrentDate());
                $em = $this->getDoctrine()->getManager();
                $em->persist($contact);
                $em->flush();
//                $emailFrom = $contact->getEmail();

                //Llenado de variable para enviar correo
//                $msg = \Swift_Message::newInstance()
//                        ->setSubject('Contactenos - PUPPYHOME.CO')
//                        ->setFrom($emailFrom)
//                        ->setTo($this->container->getParameter('puppyhome_email_to'))
//                        ->setBody($this->renderView('FrontendBundle:Default:mailContact.html.twig', array(
//                            'contact' => $contact)), 'text/html');
//                //Envio de correo y notificación de éxito
//                $this->get('mailer')->send($msg);               
                
                return $this->redirect($this->generateUrl('frontend_homepage', array(
//                            'formMail' => $formMail->createView(),
                            'pageJump' => 'contact',
//                            'formMail' => $formMail->createView(),
//                            'galeria' => $gallery,
//                            'testimonios' => $testimonio,
//                            'page' => 'index'
                )));
            } 
        

        return $this->render('FrontendBundle:Default:index.html.twig', array(
                    'pageJump' => 'contact',
                    'formMail' => $formMail->createView(),
                    'galeria' => $gallery,
                    'testimonios' => $testimonio,
                    'page' => 'index'
        ));
    }

}
