<?php

namespace WebsiteControlPanel\BackendBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase {

    public function testLoginAction() {
        $client = static::createClient();
        $crawler = $client->request('GET', '/private/login');
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'Status 200 en portada');
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Problema al cargar el login');
        $client->followRedirects(true);
    }

}
