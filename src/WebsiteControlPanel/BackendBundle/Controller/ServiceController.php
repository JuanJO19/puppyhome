<?php

namespace WebsiteControlPanel\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use WebsiteControlPanel\BackendBundle\Entity\Service;
use WebsiteControlPanel\BackendBundle\Form\ServiceType;
use WebsiteControlPanel\BackendBundle\Entity\ContentService;
use WebsiteControlPanel\BackendBundle\Form\ContentServiceType;
use WebsiteControlPanel\BackendBundle\Entity as Entity;
use WebsiteControlPanel\BackendBundle\Form\ImageType;
use Doctrine\Common\Util\Debug;


/**
 * Service controller.
 *
 */
class ServiceController extends Controller {

    /**
     * Lists all Service entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:Service')->findAll();

        return $this->render('BackendBundle:Service:index.html.twig', array(
                    'entities' => $entities,
                    'menu' => 'service',
        ));
    }

    /**
     * Creates a new Service entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Service();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('service_show', array('id' => $entity->getId())));
        }

        return $this->render('BackendBundle:Service:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Service entity.
     *
     * @param Service $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Service $entity) {
        $form = $this->createForm(new ServiceType(), $entity, array(
            'action' => $this->generateUrl('service_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Service entity.
     *
     */
    public function newAction() {
        $entity = new Service();
        $form = $this->createCreateForm($entity);

        return $this->render('BackendBundle:Service:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Service entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Service')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:Service:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Service entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Service')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:Service:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Service entity.
     *
     * @param Service $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Service $entity) {
        $form = $this->createForm(new ServiceType(), $entity, array(
            'action' => $this->generateUrl('service_update', array(
                'id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }

    /**
     * Edits an existing Service entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Service')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('service'));
        }

        return $this->render('BackendBundle:Service:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Service entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackendBundle:Service')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Service entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('service'));
    }

    /**
     * Creates a form to delete a Service entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('service_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    public function editContentAction(Request $request, $id, $leng) {

        $em = $this->getDoctrine()->getManager();

        $service = $em->getRepository('BackendBundle:Service')->find($id);
        $lengu = $em->getRepository('BackendBundle:Language')->findOneBy(array('slug' => $leng));

        if (!$service) {
            throw $this->createNotFoundException('Unable to find event entity.');
        }

        //aca iran los contenidos a editar
        //buscamos todos los leguajes disponibles en la BD
        //para cada lenguaje consultamos el contenido de la pagina solicitada

        $contentLanguage = $em->getRepository('BackendBundle:ContentService')
                ->findContent($leng, $service->getId());
        //var_dump($contentLanguage);           die();
        //si no existe contenido para la pagina solicitada en uno o varios lenguajes, lo creamos
        if (!$contentLanguage) {
            $contentLanguage = new ContentService();
            $contentLanguage->setLanguage($lengu);
            $contentLanguage->setService($service);
            $em->persist($contentLanguage);
        }

        $editForm = $this->createForm(new ContentServiceType(), $contentLanguage);
        $deleteForm = $this->createDeleteForm($id);

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('service', array('menu' => 'service')));
        }

        return $this->render('BackendBundle:Service:editContent.html.twig', array(
                    'entity' => $service,
                    'id' => $id,
                    'leng' => $leng,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }
    
    public function listImagesServiceAction($id) {
        $em = $this->getDoctrine()->getManager();
        $service = $em->getRepository('BackendBundle:Service')->find($id);
        
        if (!$service) {
            throw $this->createNotFoundException('Unable to find Service.');
        }

        //vamos a consultar las imagenes de la categoria solicitada
        $criteria = array('service' => $id);
        $orderBy = array('order' => 'ASC');
        $images = $em->getRepository('BackendBundle:Image')
                ->findBy($criteria, $orderBy);

        //creamos el formulario para la subida de imagenes
//        \Doctrine\Common\Util\Debug::dump($images, 3); die();
        $image = new Entity\Image();
        $formImage = $this->createForm(new ImageType(), $image);

        return $this->render('BackendBundle:Service:listImageProject.html.twig', array(
                    'service' => $service,
                    'images' => $images,
                    'formImage' => $formImage->createView(),
        ));
    }
    
    public function uploadImageServiceAction($id) {
        $em = $this->getDoctrine()->getManager();
        $service = $em->getRepository('BackendBundle:Service')->find($id);

        if (!$service) {
            throw $this->createNotFoundException('Unable to find Service.');
        }

        $image = new Entity\Image();
        $formImage = $this->createForm(new ImageType(), $image);

        $request = $this->getRequest();

        $formImage->handleRequest($request);

        //var_dump($_FILES);die();

        if ($formImage->isValid()) {
            //realizamos la subida del archivo
            $image->setPath($image->getImage());
            if ($image->getPath() != '') {
                $image->uploadImage($this->container->getParameter('piura_directory_images'));
            }

            //seteamos la categoria
            $image->setService($service);
            $image->setOrder(999);
            $em->persist($image);

            $em->flush();

            return $this->redirect($this
                                    ->generateUrl('backend_service_list_images', array(
                                        'id' => $service->getId())));
        } else {
            $this->get('session')->getFlashBag()
                    ->add('errorUploading', 'Formato de archivo invalido, por favor seleccione una imagen correcta');

            return $this->redirect($this
                                    ->generateUrl('backend_service_list_images', array(
                                        'id' => $service->getId())));
        }
    }
    
    public function editImageAction($id) {

        $em = $this->getDoctrine()->getManager();
        $image = $em->getRepository('BackendBundle:Image')->find($id);

        if (!$image) {
            throw $this->createNotFoundException('Unable to find Image.');
        }

        //no se instancia una nueva Imagen pues hay es que editar la que ya se tiene
        $formImage = $this->createForm(new ImageType(), $image);


        return $this->render('BackendBundle:Service:editImage.html.twig', array(
                    'image' => $image,
                    'formImage' => $formImage->createView(),
        ));
    }
    
    public function updateImageAction($id, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $image = $em->getRepository('BackendBundle:Image')->find($id);

        if (!$image) {
            throw $this->createNotFoundException('Unable to find Image.');
        }

        $formImage = $this->createForm(new ImageType(), $image);

        $formImage->handleRequest($request);

        if ($formImage->isValid()) {
            $em->persist($image);
            $em->flush();
            return $this->redirect($this
                                    ->generateUrl('backend_service_list_images', array(
                                        'id' => $image->getService()->getId())));
        }

        return $this->render('BackendBundle:Service:editImage.html.twig', array(
                    'image' => $image,
                    'formImage' => $formImage->createView(),
        ));
    }

}
