<?php

namespace WebsiteControlPanel\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use WebsiteControlPanel\BackendBundle\Form\ImageType;
use WebsiteControlPanel\BackendBundle\Entity\Worker;
use WebsiteControlPanel\BackendBundle\Form\WorkerType;
use WebsiteControlPanel\BackendBundle\Form\ContentWorkerType;
use WebsiteControlPanel\BackendBundle\Entity as Entity;
use WebsiteControlPanel\BackendBundle\Entity\ContentWorker;

/**
 * workers controller.
 * @author Juan José Molina <jmolina@kijho.com>
 * @author Juan Carlos Galvis<jgalvis@kijho.com>
 */
class WorkerController extends Controller {

    /**
     * Lists all Workers entities.
     *
     */
    public function indexAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(10);
        $paginator->setMaxPagerItems(4);
        $entities = $paginator->paginate($em->getRepository('BackendBundle:Worker')->findAll())->getResult();

        //Funcionamiento Buscador
        $search = $request->query->get('search');

        $search = $request->get('search');
        $search = trim($search);
        if ($search != "") {
            $entities = $paginator->paginate(
                            $em->getRepository('BackendBundle:Worker')
                                    ->findWorker($search))->getResult();

            return $this->render('BackendBundle:Worker:index.html.twig', array(
                        'entities' => $entities,
                        'menu' => 'worker',
                        'paginator' => $paginator,
                        'search' => $search,
                        'find' => 'find'
            ));
        }

        if ($search == null) {
            $search = "";
        }

        return $this->render('BackendBundle:Worker:index.html.twig', array(
                    'entities' => $entities,
                    'menu' => 'worker',
                    'paginator' => $paginator,
                    'search' => $search,
        ));
    }

    /**
     * Creates a new Project entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Worker();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_Worker_image', array('id' => $entity->getId())));
        }

        return $this->render('BackendBundle:Worker:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Project entity.
     *
     * @param Worker $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Worker $entity) {
        $form = $this->createForm(new WorkerType(), $entity, array(
            'action' => $this->generateUrl('worker_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Project entity.
     *
     */
    public function newAction() {
        $entity = new Worker();
        $form = $this->createCreateForm($entity);

        return $this->render('BackendBundle:Worker:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Project entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Project')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:Project:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Project entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Worker')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:Worker:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Project entity.
     *
     * @param Project $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Worker $entity) {
        $form = $this->createForm(new WorkerType(), $entity, array(
            'action' => $this->generateUrl('worker_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing project entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Worker')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('worker', array('menu' => 'worker')));
        }

        return $this->render('BackendBundle:Worker:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a project entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackendBundle:Worker')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Worker entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('worker'));
    }

    /**
     * Creates a form to delete a project entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('worker_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    public function listImageWorkerAction($id) {
        $em = $this->getDoctrine()->getManager();
        $worker = $em->getRepository('BackendBundle:Worker')->find($id);

        if (!$worker) {
            throw $this->createNotFoundException('Unable to find Worker.');
        }

        //vamos a consultar las imagenes de la categoria solicitada
        $criteria = array('worker' => $id);
        $orderBy = array('order' => 'ASC');
        $images = $em->getRepository('BackendBundle:Image')->findBy($criteria, $orderBy);

        //creamos el formulario para la subida de imagenes
        $image = new Entity\Image();
        $formImage = $this->createForm(new ImageType(), $image);

        return $this->render('BackendBundle:Worker:listImageWorker.html.twig', array(
                    'worker' => $worker,
                    'images' => $images,
                    'formImage' => $formImage->createView(),
        ));
    }

    public function updateImageAction($id, Request $request) {


        $em = $this->getDoctrine()->getManager();
        $image = $em->getRepository('BackendBundle:Image')->find($id);

        if (!$image) {
            throw $this->createNotFoundException('Unable to find Image.');
        }

        $formImage = $this->createForm(new ImageType(), $image);

        $formImage->handleRequest($request);

        if ($formImage->isValid()) {
            $em->persist($image);
            $em->flush();
            return $this->redirect($this->generateUrl('backend_Worker_image', array('id' => $image->getWorker()->getId())));
        }

        return $this->render('BackendBundle:Worker:editImage.html.twig', array(
                    'image' => $image,
                    'formImage' => $formImage->createView(),
        ));
    }

    public function editImageAction($id) {


        $em = $this->getDoctrine()->getManager();
        $image = $em->getRepository('BackendBundle:Image')->find($id);

        if (!$image) {
            throw $this->createNotFoundException('Unable to find Image.');
        }

        //no se instancia una nueva Imagen pues hay es que editar la que ya se tiene
        $formImage = $this->createForm(new ImageType(), $image);


        return $this->render('BackendBundle:Project:editImage.html.twig', array(
                    'image' => $image,
                    'formImage' => $formImage->createView(),
        ));
    }

    public function uploadImageWorkerAction($id) {
        $em = $this->getDoctrine()->getManager();
        $worker = $em->getRepository('BackendBundle:Worker')->find($id);

        if (!$worker) {
            throw $this->createNotFoundException('Unable to find Worker.');
        }

        $image = new Entity\Image();
        $formImage = $this->createForm(new ImageType(), $image);
        $request = $this->getRequest();
        $formImage->handleRequest($request);

        //var_dump($_FILES);die();

        if ($formImage->isValid()) {
            //realizamos la subida del archivo
            $image->setPath($image->getImage());
            if ($image->getPath() != '') {
                $image->uploadImage($this->container->getParameter('piura_directory_images'));
            }

            //seteamos la categoria
            $image->setWorker($worker);
            $image->setOrder(999);
            $em->persist($image);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_Worker_image', array('id' => $worker->getId())));
        } else {
            $this->get('session')->getFlashBag()->add('errorUploading', 'Formato de archivo invalido, por favor seleccione una imagen correcta');

            return $this->redirect($this->generateUrl('backend_Worker_image', array('id' => $worker->getId())));
        }
    }

    public function deleteWorkerAction(Request $request) {



        $workerId = $request->request->get('workerId');
        $response = array();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Worker')->find($workerId);

        if (!$entity) {
            $response['result'] = '__KO__';
            $response['msg'] = 'Trabajador no encontrado';
        } else {



            $em->remove($entity);
            $em->flush();

            $response['result'] = '__OK__';
            $response['msg'] = 'Trabajador eliminado';
        }

        $r = new Response(json_encode($response));
        $r->headers->set('Content-Type', 'application/json');
        return $r;
    }

    public function editContentAction(Request $request, $id, $leng) {

        $em = $this->getDoctrine()->getManager();

        $worker = $em->getRepository('BackendBundle:Worker')->find($id);
        $lengu = $em->getRepository('BackendBundle:Language')->findOneBy(array('slug' => $leng));

        if (!$worker) {
            throw $this->createNotFoundException('Unable to find event entity.');
        }

        //aca iran los contenidos a editar
        //buscamos todos los leguajes disponibles en la BD
        //para cada lenguaje consultamos el contenido de la pagina solicitada

        $contentLanguage = $em->getRepository('BackendBundle:ContentWorker')->findContent($leng, $worker->getId());
        //var_dump($contentLanguage);           die();
        //si no existe contenido para la pagina solicitada en uno o varios lenguajes, lo creamos
        if (!$contentLanguage) {
            $contentLanguage = new ContentWorker();
            $contentLanguage->setLanguage($lengu);
            $contentLanguage->setWorker($worker);
            $em->persist($contentLanguage);
        }

        $editForm = $this->createForm(new ContentWorkerType(), $contentLanguage);
        $deleteForm = $this->createDeleteForm($id);

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('worker', array('menu' => 'worker')));
        }

        return $this->render('BackendBundle:Worker:editContent.html.twig', array(
                    'entity' => $worker,
                    'id' => $id,
                    'leng' => $leng,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

}
