<?php

namespace WebsiteControlPanel\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use WebsiteControlPanel\BackendBundle\Entity\GalleryImages;
use WebsiteControlPanel\BackendBundle\Form\ImagenesType;
use WebsiteControlPanel\BackendBundle\Entity as Entity;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Raza controller.
 *
 */
class GalleryController extends Controller {

    /**
     * Lists all Project entities.
     *
     */
    public function indexAction() {

        $em = $this->getDoctrine()->getManager();
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(10);
        $paginator->setMaxPagerItems(4);
        $entities = $paginator->paginate($em->getRepository('BackendBundle:GalleryImages')->findBy(array(), array('order' => 'ASC')))->getResult();

        return $this->render('BackendBundle:Galeria:index.html.twig', array(
                    'entities' => $entities,
                    'menu' => 'galeria',
                    'paginator' => $paginator,
                   
        ));
    }  

    /**
     * Creates a form to create a Project entity.
     *
     * @param Project $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(GalleryImages $entity) {
        $form = $this->createForm(new ImagenesType(), $entity, array(
            'action' => $this->generateUrl('backend_gallery_add'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Project entity.
     *
     */
    public function newAction() {
        $entity = new Entity\GalleryImages();
        $form = $this->createCreateForm($entity);

        return $this->render('BackendBundle:Galeria:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }
    
    public function saveRegisterAction(Request $request) {        
        
       
        $em = $this->getDoctrine()->getManager();        
        $entity = new Entity\GalleryImages();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);        

        if ($form->isValid()) {          
            $em = $this->getDoctrine()->getManager();            
            $em->persist($entity);
            $em->flush();
            
            return $this->redirect($this->generateUrl('backend_gallery_index', array()));
        }

        return $this->render('BackendBundle:Galeria:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }
    
    public function orderImagesAction(Request $request) {
        $em = $this->getDoctrine()->getManager();        
        $listImage = $request->request->get('image');        
        $i = 1;
        foreach ($listImage as $imageId) {
            $imageBanner = $em->getRepository('BackendBundle:GalleryImages')->find($imageId);
            $imageBanner->setOrder($i);
            $i++;
        }
        $em->flush();
        $response['result'] = '__OK__';
        $response['msg'] = 'Orden actualizado';


        $r = new Response(json_encode($response));
        $r->headers->set('Content-Type', 'application/json');
        return $r;
    }    
        
    public function deleteAction(Request $request) {
        $response['msg'] = '';
        $response['result'] = '__OK__';
        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:GalleryImages')->find($id);
        

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find img entity.');
        }

        $em->remove($entity);
        $em->flush();
        $response = new JsonResponse(
                array('result' => '__OK__'
        ));
        
        return $response;
    }
    
    public function editAction($id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:GalleryImages')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find gallery images entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('BackendBundle:Galeria:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
        ));
    }
    
    private function createEditForm(GalleryImages $entity) {
        $form = $this->createForm(new ImagenesType(), $entity, array(
            'action' => $this->generateUrl('backend_img_update', array(
                'id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:GalleryImages')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find img gallery entity.');
        }
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            return $this->redirect($this->generateUrl('backend_gallery_index'));
        }

        return $this->render('BackendBundle:Galeria:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
        ));
    }
}
