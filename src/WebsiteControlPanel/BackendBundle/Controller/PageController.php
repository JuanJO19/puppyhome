<?php

namespace WebsiteControlPanel\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use WebsiteControlPanel\BackendBundle\Entity\Page;
use WebsiteControlPanel\BackendBundle\Form\PageType;
use WebsiteControlPanel\BackendBundle\Form\ContentPageType;
use WebsiteControlPanel\BackendBundle\Entity as Entity;

/**
 * Page controller.
 *
 */
class PageController extends Controller
{

    /**
     * Lists all Page entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:Page')->findAll();

        return $this->render('BackendBundle:Page:index.html.twig', array(
            'entities' => $entities,
            'menu'=>'pages'
        ));
    }
    /**
     * Creates a new Page entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Page();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_pages_show', array('id' => $entity->getId())));
        }

        return $this->render('BackendBundle:Page:new.html.twig', array(
            'entity' => $entity,                
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Page entity.
    *
    * @param Page $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Page $entity)
    {
        $form = $this->createForm(new PageType(), $entity, array(
            'action' => $this->generateUrl('backend_pages_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Page entity.
     *
     */
    public function newAction()
    {
        if(!$this->get('security.context')->isGranted('ROLE_USUARIO'))
        {
            return $this->redirect($this->generateUrl('backend_user_logout')); 
        }
        $entity = new Page();
        $form   = $this->createCreateForm($entity);

        return $this->render('BackendBundle:Page:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Page entity.
     *
     */
    public function showAction($id)
    {
        
        if(!$this->get('security.context')->isGranted('ROLE_USUARIO'))
        {
            return $this->redirect($this->generateUrl('backend_user_logout')); 
        }
        
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Page')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:Page:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Page entity.
     *
     */
    public function editAction($id)
    {
        //solo el administrador puede entrar aqui
        if(!$this->get('security.context')->isGranted('ROLE_USUARIO'))
        {
            return $this->redirect($this->generateUrl('backend_user_logout')); 
        }
        
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Page')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:Page:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'menu'=>'pages'
        ));
    }
    
    /**
     * Displays a form to edit an existing Page entity.
     *
     */
    public function editContentAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $page = $em->getRepository('BackendBundle:Page')->find($id);

        if (!$page) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        //aca iran los contenidos a editar
        $editableContents = array();
        
        //buscamos todos los leguajes disponibles en la BD
        $languages = $em->getRepository('BackendBundle:Language')->findAll();
        
        //para cada lenguaje consultamos el contenido de la pagina solicitada
        for($i=0; $i<count($languages); $i++)
        {
           $contentLanguage = $em->getRepository('BackendBundle:ContentPage')->findContent($languages[$i]->getSlug(), $page->getSlug());
           
           //si no existe contenido para la pagina solicitada en uno o varios lenguajes, lo creamos
           if(!$contentLanguage)
           {
              $contentLanguage = new Entity\ContentPage();
              $contentLanguage->setLanguage($languages[$i]);
              $contentLanguage->setPage($page);
              $em->persist($contentLanguage);
           }
           
           $form = $this->createForm(new ContentPageType($contentLanguage->getLanguage()->getSlug()), $contentLanguage);
           $form = $form->createView();
           
           $editContent['form'] = $form;
           $editContent['content'] = $contentLanguage;
           
           array_push($editableContents, $editContent);
        }
        $em->flush();
        
        $request = $this->getRequest();
        if($request->getMethod() == 'POST')
        {
            //para cada lenguaje verificamos el contenido que se edito
            for($i=0; $i<count($languages); $i++)
            {
                //capturamos el contenido que el usuario edito para cada lenguaje
                $parameters = $request->request->get('websitecontrolpanel_backendbundle_contentpage_'.$languages[$i]->getSlug());
                $newContent = $parameters['content'];
                
                $contentLanguage = $em->getRepository('BackendBundle:ContentPage')->findContent($languages[$i]->getSlug(), $page->getSlug());

                if($contentLanguage instanceof Entity\ContentPage)
                {
                   $contentLanguage->setContent($newContent);
                   $em->persist($contentLanguage);
                }
            }
            $em->flush();
            
            return $this->redirect($this->generateUrl('backend_pages'));
        }
        
        //consultamos que categorias de imagenes estan relacionadas a esta pagina
        $relationshipImages = $em->getRepository('BackendBundle:ImagePageRelationship')->findByPage($page->getId());   
        
        return $this->render('BackendBundle:Page:editContent.html.twig', array(
            'page'      => $page, 
            'editableContents'=> $editableContents,
            'relationshipImages'=>$relationshipImages,
        ));
    }

    /**
    * Creates a form to edit a Page entity.
    *
    * @param Page $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Page $entity)
    {
        $form = $this->createForm(new PageType(), $entity, array(
            'action' => $this->generateUrl('backend_pages_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Page entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Page')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('backend_pages_edit', array('id' => $id)));
        }

        return $this->render('BackendBundle:Page:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Page entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackendBundle:Page')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Page entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('backend_pages'));
    }

    /**
     * Creates a form to delete a Page entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_pages_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
