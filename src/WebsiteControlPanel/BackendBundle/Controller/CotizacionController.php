<?php

namespace WebsiteControlPanel\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use WebsiteControlPanel\BackendBundle\Entity\Respuesta;
use WebsiteControlPanel\BackendBundle\Form\RespuestaType;
use WebsiteControlPanel\BackendBundle\Entity as Entity;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Raza controller.
 *
 */
class CotizacionController extends Controller {

    /**
     * Lists all Project entities.
     *
     */
    public function indexAction() {

        $em = $this->getDoctrine()->getManager();
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(10);
        $paginator->setMaxPagerItems(4);
        $entities = $paginator->paginate($em->getRepository('BackendBundle:ContactoCotizar')->findAll())->getResult();

        return $this->render('BackendBundle:Cotizaciones:index.html.twig', array(
                    'entities' => $entities,
                    'menu' => 'cotizaciones',
                    'paginator' => $paginator,
        ));
    }

    /**
     * Creates a form to create a Project entity.
     *
     * @param Project $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Respuesta $entity) {
        $form = $this->createForm(new RespuestaType(), $entity, array(
            'action' => $this->generateUrl('backend_cotizaciones_respuesta_add', array('id' => $id)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Responder'));

        return $form;
    }

    /**
     * Displays a form to create a new Project entity.
     *
     */
    public function newAction($id) {

        $em = $this->getDoctrine()->getManager();
        $entitie = $em->getRepository('BackendBundle:ContactoCotizar')->find($id);
        $temp = $entitie->getRespuesta();
        if ($temp != null) {
            $resp = $em->getRepository('BackendBundle:Respuesta')->find($temp);
            $entity = $resp;
        } else {
            $entity = new Respuesta();
        }
        $form = $this->createForm(new RespuestaType(), $entity);

        return $this->render('BackendBundle:Cotizaciones:newRespuesta.html.twig', array(
                    'entity' => $entity,
                    'entitie' => $entitie,
                    'form' => $form->createView(),
        ));
    }

    public function saveRespuestaAction(Request $request, $id) {


        $em = $this->getDoctrine()->getManager();
        $entity = new Respuesta();
        $form = $this->createForm(new RespuestaType(), $entity);
        $form->handleRequest($request);
        $entitie = $em->getRepository('BackendBundle:ContactoCotizar')->find($id);

        if ($form->isValid()) {
            $em->persist($entity);
            $entitie->setRespuesta($entity);
            $entitie->setEstado(true);
            $em->persist($entitie);
            $em->flush();


            $message = \Swift_Message::newInstance()
                    ->setSubject('Cotización PUPPYHOME.CO')
                    ->setFrom($this->container->getParameter('puppyhome_email_from'))
                    ->setTo($entitie->getEmail())
                    ->setBody($this->renderView('BackendBundle:Cotizaciones:mailContact.html.twig', array(
                        'entitie' => $entitie,
                        'entity' => $entity)), 'text/html');
            $this->get('mailer')->send($message);
//            $this->get('session')->setFlash('cotizacion-not', 'Cotización enviada');

            return $this->redirect($this->generateUrl('backend_cotizaciones_index', array()));
//            return $this->render('BackendBundle:Cotizaciones:mailContact.html.twig', array(
//                        'entitie' => $entitie,
//                        'entity' => $entity));
        }

        return $this->render('BackendBundle:Cotizaciones:newRespuesta.html.twig', array(
                    'entity' => $entity,
                    'entitie' => $entitie,
                    'form' => $form->createView(),
        ));
    }

    public function deleteAction(Request $request) {
        $response['msg'] = '';
        $response['result'] = '__OK__';
        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:ContactoCotizar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find cotización entity.');
        }

        $em->remove($entity);
        $em->flush();
        $response = new JsonResponse(
                array('result' => '__OK__'
        ));
        
        return $response;
    }

}
