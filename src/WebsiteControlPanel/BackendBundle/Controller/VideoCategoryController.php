<?php

namespace WebsiteControlPanel\BackendBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use WebsiteControlPanel\BackendBundle\Entity\VideoCategory;
use WebsiteControlPanel\BackendBundle\Entity as Entity;
use WebsiteControlPanel\BackendBundle\Form\VideoCategoryType;
use WebsiteControlPanel\BackendBundle\Form\VideoType;

/**
 * VideoCategory controller.
 *
 */
class VideoCategoryController extends Controller
{

    /**
     * Lists all VideoCategory entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:VideoCategory')->findAll();

        return $this->render('BackendBundle:VideoCategory:index.html.twig', array(
            'entities' => $entities,
            'menu'=>'multimedia',
            'submenu'=>'videos'
        ));
    }
    /**
     * Creates a new VideoCategory entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new VideoCategory();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_video_category_show', array('id' => $entity->getId())));
        }

        return $this->render('BackendBundle:VideoCategory:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a VideoCategory entity.
    *
    * @param VideoCategory $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(VideoCategory $entity)
    {
        $form = $this->createForm(new VideoCategoryType(), $entity, array(
            'action' => $this->generateUrl('backend_video_category_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new VideoCategory entity.
     *
     */
    public function newAction()
    {
        if(!$this->get('security.context')->isGranted('ROLE_USUARIO'))
        {
            return $this->redirect($this->generateUrl('backend_user_logout')); 
        }
        
        $entity = new VideoCategory();
        $form   = $this->createCreateForm($entity);

        return $this->render('BackendBundle:VideoCategory:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a VideoCategory entity.
     *
     */
    public function showAction($id)
    {
        if(!$this->get('security.context')->isGranted('ROLE_USUARIO'))
        {
            return $this->redirect($this->generateUrl('backend_user_logout')); 
        }
        
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:VideoCategory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find VideoCategory entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:VideoCategory:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing VideoCategory entity.
     *
     */
    public function editAction($id)
    {
        
              if(!$this->get('security.context')->isGranted('ROLE_USUARIO'))
        {
            return $this->redirect($this->generateUrl('backend_user_logout')); 
        }
        
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:VideoCategory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find VideoCategory entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:VideoCategory:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a VideoCategory entity.
    *
    * @param VideoCategory $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(VideoCategory $entity)
    {
        $form = $this->createForm(new VideoCategoryType(), $entity, array(
            'action' => $this->generateUrl('backend_video_category_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    
    /**
     * Edits an existing VideoCategory entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:VideoCategory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find VideoCategory entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

//            return $this->redirect($this->generateUrl('backend_video_category_edit', array('id' => $id)));
            return $this->redirect($this->generateUrl('backend_video_category'));
        }

        return $this->render('BackendBundle:VideoCategory:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a VideoCategory entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackendBundle:VideoCategory')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find VideoCategory entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('backend_video_category'));
    }

    /**
     * Creates a form to delete a VideoCategory entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_video_category_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    
    public function listVideosAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('BackendBundle:VideoCategory')->find($id);

        if (!$category) {
            throw $this->createNotFoundException('Unable to find VideoCategory entity.');
        }
        
        //$search = array('category'=>$category->getId());
        $criteria= array('category'=>$id);
        $orderBy = array('order'=> 'ASC');        
        $listVideos = $em->getRepository('BackendBundle:Video')->findBy($criteria, $orderBy);        
        
        $video = new Entity\Video();
        $formVideo = $this->createForm(new VideoType(), $video);
        
        
            return $this->render('BackendBundle:VideoCategory:listVideos.html.twig', array(
            'category'      => $category,
            'listVideos'   => $listVideos,
            'formVideo' =>$formVideo->createView(),
           
        ));
                

        
       
    }
    
    public function uploadVideoAction($id)
    {
        $em = $this->getDoctrine()->getManager();        
        $category = $em->getRepository('BackendBundle:VideoCategory')->find($id);

        if (!$category) {
            throw $this->createNotFoundException('Unable to find VideoCategory entity.');
        }
        
        $request = $this->getRequest();        
        $video = new Entity\Video();
        $formVideo = $this->createForm(new VideoType(), $video);
        $formVideo->handleRequest($request);
        
        if($formVideo->isValid())
        {
            $urlExplode = explode('/', $video->getUrl());
            //\Doctrine\Common\Util\Debug::dump($urlExplode, 3); die();
            if(is_array($urlExplode) && !empty($urlExplode)) {                
                $video->setPath($urlExplode[3]);
                           
            }
            
            $video->setCategory($category);
            $video->setOrder(999);
            $em->persist($video);          
            $em->flush();            
            $this->get('session')->getFlashBag()->add('msgUploadingVideo',
                'Video guardado correctamente');
            return $this->redirect($this->generateUrl('backend_video_category_list_videos', array('id' => $id)));
        }
        else
        {
            $this->get('session')->getFlashBag()->add('errorUploadingVideo',
                'Url incorrecta, intente de nuevo');
            return $this->redirect($this->generateUrl('backend_video_category_list_videos', array('id' => $id)));
        }
    } 
    
    
    function removeVideoAction(Request $request){
        
        $videoId = $request->request->get('idVideo');
        $response = array();
        $em = $this->getDoctrine()->getManager();

        $video = $em->getRepository('BackendBundle:Video')->find($videoId);
        
        if (!$video){
            
            $response['result']='__KO__';
            $response['msg']='!video Not Fount!';
            
        }
        else
        {
            $em->remove($video);
            $em->flush();

            $response['result']='__OK__';
            $response['msg']='!el video fue borrado exitasamente';
        
        }
        
        $r = new Response(json_encode($response));
        $r->headers->set('Content-Type', 'application/json');
        return($r);
        
        
    }

  }

