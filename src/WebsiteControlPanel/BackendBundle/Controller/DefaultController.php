<?php

namespace WebsiteControlPanel\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;

class DefaultController extends Controller {

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        //Calculando total de visitas al mes 
        $timezone = new \DateTimeZone('America/Bogota');
        $dateStartMonth = new \DateTime(date('Y-m') . "-01 00:00:00");
        $dateStartMonth->setTimezone($timezone);
        $dateEndMonth = new \DateTime(date('Y-m') . "-01 00:00:00 + 1 month");
        $dateEndMonth->setTimezone($timezone);
        $totalmonthall = $em->getRepository('BackendBundle:View')
                ->findViewThisMonth($dateStartMonth, $dateEndMonth);
        $totalmonth = count($totalmonthall);

        //Calculando total de visitas al dia
        $dateStartToday = new \DateTime(date('Y-m-d') . "00:00:00");
        $dateStartToday->setTimezone($timezone);
        $dateEndToday = new \DateTime(date('Y-m-d') . "00:00:00 + 1 day");
        $dateEndToday->setTimezone($timezone);
        $totalTodayall = $em->getRepository('BackendBundle:View')
                ->findViewThisMonth($dateStartToday, $dateEndToday);
        $totalToday = count($totalTodayall);

        //Calculando total de visitas a la página
        $totalAll = $em->getRepository('BackendBundle:View')->findAll();
        $total = count($totalAll);
        return $this->render('BackendBundle:Default:index.html.twig', array(
                    'menu' => 'home',
                    'totaltoday' => $totalToday,
                    'totalmonth' => $totalmonth,
                    'total' => $total,
        ));
    }

    public function loginAction() {
        $peticion = $this->getRequest();
        $sesion = $peticion->getSession();

        $error = $peticion->attributes->get(
                SecurityContext::AUTHENTICATION_ERROR, $sesion->get(SecurityContext::AUTHENTICATION_ERROR)
        );
        return $this->render('BackendBundle:Default:login.html.twig', array(
                    'last_username' => $sesion->get(SecurityContext::LAST_USERNAME),
                    'error' => $error));
    }

}
