<?php

namespace WebsiteControlPanel\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use WebsiteControlPanel\BackendBundle\Entity\Raza;
use WebsiteControlPanel\BackendBundle\Form\RazaType;
use WebsiteControlPanel\BackendBundle\Form\ImageSliderRazaType;
use WebsiteControlPanel\BackendBundle\Entity as Entity;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Raza controller.
 *
 */
class RazaController extends Controller {

    /**
     * Lists all Project entities.
     *
     */
    public function indexAction() {

        $em = $this->getDoctrine()->getManager();
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(10);
        $paginator->setMaxPagerItems(4);
        $entities = $paginator->paginate($em->getRepository('BackendBundle:Raza')->findAll())->getResult();

        return $this->render('BackendBundle:Razas:index.html.twig', array(
                    'entities' => $entities,
                    'menu' => 'razas',
                    'paginator' => $paginator,
        ));
    }

    /**
     * Creates a form to create a Project entity.
     *
     * @param Project $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Raza $entity) {
        $form = $this->createForm(new RazaType(), $entity, array(
            'action' => $this->generateUrl('razas_add'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Project entity.
     *
     */
    public function newAction() {
        $entity = new Raza();
        $form = $this->createCreateForm($entity);

        return $this->render('BackendBundle:Razas:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    public function newImageSliderAction($id) {
        $em = $this->getDoctrine()->getManager();
        $temp = $em->getRepository('BackendBundle:ImageSliderRaza')->findOneBy(array('raza'=> $id));
        $entity = $temp != null ? $temp : new Entity\ImageSliderRaza;
        $form = $this->createForm(new ImageSliderRazaType(), $entity);

        $entities = $em->getRepository('BackendBundle:Raza')->find($id);


        return $this->render('BackendBundle:Razas:newImagesSlider.html.twig', array(
                    'entity' => $entity,
                    'entities' => $entities,
                    'form' => $form->createView(),
        ));
    }

    public function saveRegisterAction(Request $request) {


        $em = $this->getDoctrine()->getManager();
        $entity = new Raza();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_razas_index', array()));
        }

        return $this->render('BackendBundle:Razas:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    public function saveRegisterSliderAction(Request $request, $id) {


        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('BackendBundle:Raza')->find($id);
        $temp = $em->getRepository('BackendBundle:ImageSliderRaza')->findOneBy(array('raza'=> $id));
        $entity = $temp != null ? $temp : new Entity\ImageSliderRaza;
        $form = $this->createForm(new ImageSliderRazaType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setRaza($entities);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_razas_index', array()));
        }

        return $this->render('BackendBundle:Razas:newImagesSlider.html.twig', array(
                    'entity' => $entity,
                    'entities' => $entities,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Service entity.
     *
     */
    public function editAction($id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Raza')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Raza entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('BackendBundle:Razas:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
        ));
    }
   
    /**
     * Displays a form to edit an existing Service entity.
     *
     */
    public function viewAction($id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Raza')->find($id);

        return $this->render('BackendBundle:Razas:show.html.twig', array(
                    'entity' => $entity                   
        ));
    }

    /**
     * Creates a form to edit a Service entity.
     *
     * @param Service $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Raza $entity) {
        $form = $this->createForm(new RazaType(), $entity, array(
            'action' => $this->generateUrl('backend_razas_update', array(
                'id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }

    /**
     * Edits an existing Service entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Raza')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find raza entity.');
        }
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            return $this->redirect($this->generateUrl('backend_razas_index'));
        }

        return $this->render('BackendBundle:Razas:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
        ));
    }
    
    public function deleteAction(Request $request) {
        $response['msg'] = '';
        $response['result'] = '__OK__';
        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Raza')->find($id);
        

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find raza entity.');
        }

        $em->remove($entity);
        $em->flush();
        $response = new JsonResponse(
                array('result' => '__OK__'
        ));
        
        return $response;
    }

}
