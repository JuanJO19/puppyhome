<?php

namespace WebsiteControlPanel\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebsiteControlPanel\BackendBundle\Entity as Entity;


class ImageSliderRazaType extends AbstractType {   

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder               
                
                ->add('nombreUno', 'text', array('required' => true,
                    'label' => 'Nombre',
                    'attr' => array('minlength' => 2, 'maxlength' => 15)))                
                 ->add('imagenPathUno', 'text', array('required' => false, 'label' => 'Imagen Uno'))
                ->add('nombreDos', 'text', array('required' => true,
                    'label' => 'Nombre',
                    'attr' => array('minlength' => 2, 'maxlength' => 15)))                
                 ->add('imagenPathDos', 'text', array('required' => false, 'label' => 'Imagen Dos'))
                ->add('nombreTres', 'text', array('required' => true,
                    'label' => 'Nombre',
                    'attr' => array('minlength' => 2, 'maxlength' => 15)))                
                 ->add('imagenPathTres', 'text', array('required' => false, 'label' => 'Imagen Tres'))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'WebsiteControlPanel\BackendBundle\Entity\ImageSliderRaza'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'puppy_home_images_slider_raza';
    }

}
