<?php

namespace WebsiteControlPanel\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebsiteControlPanel\BackendBundle\Entity as Entity;


class ImagenesType extends AbstractType {   

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                
                ->add('descripcion', 'text', array('required' => false,
                     'label' => 'Descripcion Breve',
                    'attr' => array('minlength' => 2, 'maxlength' => 25)                   
                    ))
                ->add('nombre', 'text', array('required' => true,
                    'label' => 'Nombre',
                    'attr' => array('minlength' => 2, 'maxlength' => 15)))                
                 ->add('imagenPath', 'text', array('required' => false, 'label' => 'Imagen'))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'WebsiteControlPanel\BackendBundle\Entity\GalleryImages'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'puppy_home_imagenes';
    }

}
