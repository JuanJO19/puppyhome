<?php

namespace WebsiteControlPanel\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebsiteControlPanel\BackendBundle\Entity as Entity;
use Doctrine\ORM\EntityRepository;

class RazaType extends AbstractType {   

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
              
                ->add('tipo', 'choice', array(
                    'choices' => array(
                        Entity\Raza::RAZA_PEQUEÑA => 'Raza Pequeña',
                        Entity\Raza::RAZA_MEDIANA => 'Raza Mediana',
                        Entity\Raza::RAZA_GRANDE => 'Raza Grande'),
                    'label' => '',
                ))
                ->add('descripcion', 'textarea', array('required' => false))
                ->add('nombre', 'text', array('required' => true,
                    'label' => '',
                    'attr' => array('minlength' => 2, 'maxlength' => 30)))
                ->add('cantidadCachorros', 'text', array('required' => true))
                 ->add('imagenPath', 'text', array('required' => false, 'label' => 'Imagen'))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'WebsiteControlPanel\BackendBundle\Entity\Raza'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'puppy_home_raza';
    }

}
