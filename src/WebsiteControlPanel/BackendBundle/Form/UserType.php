<?php

namespace WebsiteControlPanel\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
        ->add('email','email', array('required'=>true))
        ->add('name','text', array('required'=>true))
        ->add('password','password', array('required'=>true));        
        
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'WebsiteControlPanel\BackendBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'websitecontrolpanel_backendbundle_user';
    }

}
