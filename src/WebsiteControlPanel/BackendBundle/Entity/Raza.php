<?php

namespace WebsiteControlPanel\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use AdminLTE\AdminThemeBundle\Model\UserInterface as adminLTEUserInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Raza
 * @ORM\Table(name="raza", uniqueConstraints={@ORM\UniqueConstraint(name="raza_name__constraint", columns={"raza_name"})})
 * @ORM\Entity
 * @author Juan Molina <jmolina@kijho.com> 02/11/2015
 */
class Raza {

    /**
     * Constantes para los tamaños de la raza
     */
    const RAZA_PEQUEÑA = 0;
    const RAZA_MEDIANA = 1;
    const RAZA_GRANDE = 2;
    
    /**
     * @ORM\Id
     * @ORM\Column(name="raza_id", type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * Nick name para identificar el usuario en el sistema
     * @ORM\Column(name="raza_name", type="string", length=255, nullable=false)
     */
    protected $nombre;
    
    /**
     * cantidad de cachorros de dicha raza
     * @ORM\Column(name="raza_cant", type="integer", nullable=false)
     */
    protected $cantidadCachorros;

    /**
     * Contrasena del usuario
     * @ORM\Column(name="raza_descripcion", type="text", nullable=true)
     */
    protected $descripcion;  
    
     /**
     * Estado del usuario en el sistema (0 = pequeña, 1 = mediana, 2 = grande)
     * @ORM\Column(name="raza_tipo", type="integer", nullable=true)
     */
    protected $tipo;
    
    /**
     * Instancia de un archivo de imagen para permitir la subida de la foto de la raza
     * @Assert\File(maxSize="4M", mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}, mimeTypesMessage = "Extensión de archivo inválida (.PNG - .JPEG - .PJPEG)")
     */
    protected $imagen;

    /**
     * Nombre de la imagen de la raza
     * @ORM\Column(name="raza_image_path", type="string", length=100, nullable=true) 
     */
    protected $imagenPath;

   
    
    public function getTextTipo($tipo = null) {
        $text = '';

        if (!$tipo) {
            $tipo = $this->tipo;
        }
        
        switch ($tipo) {
            case self::RAZA_PEQUEÑA: $text = 'Pequeña';
                break;
            case self::RAZA_MEDIANA: $text = 'Mediana';
                break;
            case self::RAZA_GRANDE: $text = 'Grande';
                break;
        }
        return $text;
    }

    public function serialize() {
        return serialize(array(
            $this->id,
        ));
    }

    public function unserialize($serialized) {
        list (
                $this->id,
                ) = unserialize($serialized);
    }

    function getId() {
        return $this->id;
    }  
    
    function getNombre() {
        return $this->nombre;
    } 
    
    function getCantidadCachorros() {
        return $this->cantidadCachorros;
    }  

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }
    
    function setCantidadCachorros($cantidadCachorros) {
        $this->cantidadCachorros = $cantidadCachorros;
    }
    
    function getDescripcion() {
        return $this->descripcion;
    }  

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }
    
    function getTipo() {
        return $this->tipo;
    }  

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }
    
    public function getImagenPath() {
        return $this->imagenPath;
    }    
   
    public function setImagenPath($imagenPath) {
        $this->imagenPath = $imagenPath;
    }    
    
    public function getAvatar() {
        return $this->getImagePath();
    }
}
