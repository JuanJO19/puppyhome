<?php

namespace WebsiteControlPanel\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use AdminLTE\AdminThemeBundle\Model\UserInterface as adminLTEUserInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * entidad paa guardar los registros de las cotizaciones
 * @ORM\Table(name="contacto_cotizar")
 * @ORM\Entity
 * @author Juan Molina <jmolina@kijho.com> 02/11/2015
 */
class ContactoCotizar {
    
    /**
     * @ORM\Id
     * @ORM\Column(name="conco_id", type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * nombre de la persona que va a cotizar
     * @ORM\Column(name="conco_name", type="string", length=255, nullable=false)
     */
    protected $nombreCompleto;
    
    /**
     * cantidad de cachorros de dicha raza
     * @ORM\Column(name="conco_email", type="string", nullable=false)
     */
    protected $email;
    
    /**
     * numero telefonico del usuario
     * @ORM\Column(name="conco_tel", type="integer", nullable=true)
     */
    protected $telefono;  
    
     /**
     * direccion de la persona
     * @ORM\Column(name="conco_dir", type="string", nullable=false)
     */
    protected $direccion;
     
    /**
     * mensaje
     * @ORM\Column(name="conco_mensaje", type="text", nullable=false)
     */
    protected $mensaje;
    
    /**
     * estado para saber si la cotizacion fue respondida o no
     * @ORM\Column(name="conco_estado", type="boolean", nullable=false)
     */
    protected $estado;
    
     /**
     * foranea con raza para saber a cual se le asigna la cotizacion
     * @ORM\ManyToOne(targetEntity="Raza")
     * @ORM\JoinColumn(name="conco_raza_id", referencedColumnName="raza_id")
     */
    protected $raza;
     
    /**
     * foranea con respuesta para saber si fue respondida o no
     * @ORM\OneToOne(targetEntity="Respuesta", orphanRemoval=true)
     * @ORM\JoinColumn(name="conco_resp_id", referencedColumnName="res_id")
     */
    protected $respuesta;

    public function serialize() {
        return serialize(array(
            $this->id,
        ));
    }

    public function unserialize($serialized) {
        list (
                $this->id,
                ) = unserialize($serialized);
    }

    function getId() {
        return $this->id;
    }  
    
    function getNombreCompleto() {
        return $this->nombreCompleto;
    }    

    function setNombreCompleto($nombreCompleto) {
        $this->nombreCompleto = $nombreCompleto;
    }
    
    function getEmail() {
        return $this->email;
    }    

    function setEmail($email) {
        $this->email = $email;
    }
    
    function getTelefono() {
        return $this->telefono;
    }    

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }
    
    function getDireccion() {
        return $this->direccion;
    }    

    function setDireccion($direccion) {
        $this->direccion = $direccion;
    }
    
    function getMensaje() {
        return $this->mensaje;
    }    

    function setMensaje($mensaje) {
        $this->mensaje = $mensaje;
    }
    
     function setRaza(Raza $raza = null) {
        $this->raza = $raza;
    }
    
    function getRaza() {
        return $this->raza;
    }
    
    function setRespuesta(Respuesta $respuesta = null) {
        $this->respuesta = $respuesta;
    }
    
    function getRespuesta() {
        return $this->respuesta;
    }
    
    function setEstado($estado) {
        $this->estado = $estado;
    }
    
    function getEstado() {
        return $this->estado;
    }
}
