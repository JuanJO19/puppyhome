<?php

namespace WebsiteControlPanel\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use WebsiteControlPanel\BackendBundle\Util\Util;

/**
 * views
 *
 * @ORM\Table(name="view")
 * @ORM\Entity(repositoryClass="WebsiteControlPanel\BackendBundle\Entity\ViewRepository")
 * @ORM\HasLifecycleCallbacks
 */
class View {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;    

    /**
     * @var integer
     *
     * @ORM\Column(name="Total", type="integer")
     */
    private $total; 

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set total
     *
     * @param integer $total
     * @return Total
     */
    public function setTotal($total) {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return integer 
     */
    public function getTotal() {
        return $this->total;
    }  

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Date
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate() {
        return $this->date;
    }

    /**
     *  Set Page initial status before persisting 
     * @ORM\PrePersist
     */
    public function setDefaults() {
        if ($this->date === null) {
            $this->setDate(Util::getCurrentDate());
        }
    }

}
