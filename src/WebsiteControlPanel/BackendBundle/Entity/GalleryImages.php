<?php

namespace WebsiteControlPanel\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use AdminLTE\AdminThemeBundle\Model\UserInterface as adminLTEUserInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Raza
 * @ORM\Table(name="galleryImages")
 * @ORM\Entity
 * @author Juan Molina <jmolina@kijho.com> 02/11/2015
 */
class GalleryImages {

    
    /**
     * @ORM\Id
     * @ORM\Column(name="img_id", type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * Nick name para identificar el usuario en el sistema
     * @ORM\Column(name="img_name", type="string", length=255, nullable=false)
     */
    protected $nombre;    
   

    /**
     * Contrasena del usuario
     * @ORM\Column(name="img_descripcion", type="text", nullable=true)
     */
    protected $descripcion;    
    
    /**
     * Instancia de un archivo de imagen para permitir la subida de la foto de la raza
     * @Assert\File(maxSize="4M", mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}, mimeTypesMessage = "Extensión de archivo inválida (.PNG - .JPEG - .PJPEG)")
     */
    protected $imagen;

    /**
     * Nombre de la imagen de la raza
     * @ORM\Column(name="img_image_path", type="string", length=100, nullable=true) 
     */
    protected $imagenPath;
    
     /**
     * Orden de la imagen
     * @ORM\Column(name="img_order", type="integer", nullable=true)
     */
    protected $order;


    public function serialize() {
        return serialize(array(
            $this->id,
        ));
    }

    public function unserialize($serialized) {
        list (
                $this->id,
                ) = unserialize($serialized);
    }

    function getId() {
        return $this->id;
    }  
    
    function getNombre() {
        return $this->nombre;
    } 

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }    
   
    function getDescripcion() {
        return $this->descripcion;
    }  

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }
    
    public function getImagenPath() {
        return $this->imagenPath;
    }
    
    public function setImagenPath($imagenPath) {
        $this->imagenPath = $imagenPath;
    }
    
    public function getAvatar() {
        return $this->getImagePath();
    }
    
    function getOrder() {
        return $this->order;
    }

    function setOrder($order) {
        $this->order = $order;
    }
}
