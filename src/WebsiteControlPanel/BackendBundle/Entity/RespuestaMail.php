<?php

namespace WebsiteControlPanel\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Raza
 * @ORM\Table(name="respuestaMail")
 * @ORM\Entity
 * @author Juan Molina <jmolina@kijho.com> 05/01/2016
 */
class RespuestaMail {
    
    /**
     * @ORM\Id
     * @ORM\Column(name="res_id", type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * Nick name para identificar el usuario en el sistema
     * @ORM\Column(name="res_msj", type="text", nullable=false)
     */
    protected $mensaje;  


    function getId() {
        return $this->id;
    }  
    
    function getMensaje() {
        return $this->mensaje;
    }  

    function setMensaje($mensaje) {
        $this->mensaje = $mensaje;
    }   
   
}
