<?php

namespace WebsiteControlPanel\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use AdminLTE\AdminThemeBundle\Model\UserInterface as adminLTEUserInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Raza
 * @ORM\Table(name="images_slider_razas")
 * @ORM\Entity
 * @author Juan Molina <jmolina@kijho.com> 02/11/2015
 */
class ImageSliderRaza {

    
    /**
     * @ORM\Id
     * @ORM\Column(name="img_id", type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * nombre de a imagen
     * @ORM\Column(name="img_name_uno", type="string", length=255, nullable=false)
     */
    protected $nombreUno;      
    
    /**
     * path de la imagen
     * @ORM\Column(name="img_image_path_uno", type="string", length=100, nullable=true) 
     */
    protected $imagenPathUno;
    /**
     * nombre de a imagen
     * @ORM\Column(name="img_name_dos", type="string", length=255, nullable=false)
     */
    protected $nombreDos;      
    
    /**
     * path de la imagen
     * @ORM\Column(name="img_image_path_dos", type="string", length=100, nullable=true) 
     */
    protected $imagenPathDos;
    /**
     * nombre de a imagen
     * @ORM\Column(name="img_name_tres", type="string", length=255, nullable=false)
     */
    protected $nombreTres;      
    
    /**
     * path de la imagen
     * @ORM\Column(name="img_image_path_tres", type="string", length=100, nullable=true) 
     */
    protected $imagenPathTres;
    
    /**
     * Instancia de un archivo de imagen para permitir la subida de la foto de la raza
     * @Assert\File(maxSize="4M", mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}, mimeTypesMessage = "Extensión de archivo inválida (.PNG - .JPEG - .PJPEG)")
     */
    protected $imagen;
    
     /**
     * foranea a raza que van asignadas las fotos
     * @ORM\ManyToOne(targetEntity="Raza")
     * @ORM\JoinColumn(name="raza_slider_fancy_raza", referencedColumnName="raza_id", onDelete="CASCADE")
     */
    protected $raza;
    
   
    public function serialize() {
        return serialize(array(
            $this->id,
        ));
    }

    public function unserialize($serialized) {
        list (
                $this->id,
                ) = unserialize($serialized);
    }

    function getId() {
        return $this->id;
    }  
    
    function getNombreUno() {
        return $this->nombreUno;
    } 
    
    function setRaza(raza $raza = null) {
        $this->raza = $raza;
    }
    
    public function getRaza() {
        return $this->raza;
    }    
   
    function getNombreDos() {
        return $this->nombreDos;
    } 
    
    function getNombreTres() {
        return $this->nombreTres;
    } 

    function setNombreUno($nombreUno) {
        $this->nombreUno = $nombreUno;
    }    
    function setNombreDos($nombreDos) {
        $this->nombreDos = $nombreDos;
    }    
    function setNombreTres($nombreTres) {
        $this->nombreTres = $nombreTres;
    }   
    
    public function getImagenPathUno() {
        return $this->imagenPathUno;
    }
    
    public function getImagenPathDos() {
        return $this->imagenPathDos;
    }
    
    public function getImagenPathTres() {
        return $this->imagenPathTres;
    }
    
    public function setImagenPathUno($imagenPathUno) {
        $this->imagenPathUno = $imagenPathUno;
    }
    
    public function setImagenPathDos($imagenPathDos) {
        $this->imagenPathDos = $imagenPathDos;
    }
    
    public function setImagenPathTres($imagenPathTres) {
        $this->imagenPathTres = $imagenPathTres;
    }
    
    public function getAvatar() {
        return $this->getImagePath();
    }
    
    
}
