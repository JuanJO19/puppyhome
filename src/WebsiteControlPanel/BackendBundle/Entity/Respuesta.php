<?php

namespace WebsiteControlPanel\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use AdminLTE\AdminThemeBundle\Model\UserInterface as adminLTEUserInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Raza
 * @ORM\Table(name="respuesta")
 * @ORM\Entity
 * @author Juan Molina <jmolina@kijho.com> 02/11/2015
 */
class Respuesta {
    
    /**
     * @ORM\Id
     * @ORM\Column(name="res_id", type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * Nick name para identificar el usuario en el sistema
     * @ORM\Column(name="res_msj", type="text", nullable=false)
     */
    protected $mensaje;  


    function getId() {
        return $this->id;
    }  
    
    function getMensaje() {
        return $this->mensaje;
    }  

    function setMensaje($mensaje) {
        $this->mensaje = $mensaje;
    }   
   
}
