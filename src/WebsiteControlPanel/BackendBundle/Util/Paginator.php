<?php
namespace WebsiteControlPanel\BackendBundle\Util;

/** 
 * Paginator class
 * @author Cesar Naranjo - Kijho Technologies <cnaranjo@kijho.com>
 * @since 1.0 17/02/2014
 */
class Paginator
{    
    const IMAGE_ARROW_UP = 'up_arrow.gif';
    const IMAGE_ARROW_DOWN = 'down_arrow.gif';
    
    const REQUEST_TYPE_ARRAY = 'array';
    const REQUEST_TYPE_REQUEST = 'request';
    
        
    /**
     * Esta funcion permite filtrar de un arreglo los parametros de busqueda u ordemaniento
     * con los indices indicados en otro arreglo u objeto request, metodo utilizado para los paginadores
     * @author Cesar Naranjo - Kijho Technologies <cnaranjo@kijho.com>
     * @since 1.0 17/02/2014
     * @param array[string] $index arreglo con las claves que contiene el arreglo de parametros
     * @param \Symfony\Component\HttpFoundation\Request $request peticion $_POST, array[string] parametros sin filtrar
     * @return array[string] arreglo filtrado
     */
    public static function filterParameters($index, $parametersRequest, $type, $castToInteger = null) 
    {
        $arrayFiltered = array();
        foreach($index as $item)
        {
      
            if($type == self::REQUEST_TYPE_ARRAY)
            {
                if(isset($parametersRequest[$item]) && $parametersRequest[$item] != '')
                {
                    if(!is_array ( $parametersRequest[$item] ))
                    {
                        $filterItem = preg_replace('@[ ]{2,}@',' ',trim($parametersRequest[$item]));
                
                    }
                }
                if($castToInteger == true)
                {
                    $filterItem = (int)$filterItem;
                   
                }
                if(isset($filterItem) && $filterItem != '')
                {
                    $arrayFiltered[$item] = $filterItem;
                    $filterItem = '';
                }
            }
            elseif($type == self::REQUEST_TYPE_REQUEST)
            {
                $filterItem = preg_replace('@[ ]{2,}@',' ',trim($parametersRequest->query->get($item)));
                
                if($castToInteger == true)
                {
                    $filterItem = (int)$filterItem;
                }
                if(isset($filterItem) && $filterItem != '')
                {
                    $arrayFiltered[$item] = $filterItem;
                }
            }
        }
        return $arrayFiltered;
    }
    
    /**
     * Esta funcion permite construir una url, correspondiente a una peticion $_GET a partir
     * de un arreglo de parametros cualquiera.
     * @author Cesar Naranjo - Kijho Technologies <cnaranjo@kijho.com>
     * @since 1.0 18/02/2014
     * @param array[string] $index indices del arreglo de parametros
     * @param array[string] $search arreglo de parametros
     * @return string cadena de texto con la url correspondiente
     */
    public static function getUrlFromParameters($index, $search)
    {   
        $url = '';
        foreach($index as $item)
        {
            if(isset($search[$item]) && $search[$item] != '')
            {
                if(!is_array($search[$item]))
                {
                    $url = $url.'&'.$item."=".$search[$item];
                }
            }
        }
        return $url;
    }
    
    /**
     * Esta funcion prmite construir un arreglo el cual contiene los datos para realizar
     * las tareas de ordenamiento en un listado cualquiera
     * @author Cesar Naranjo - Kijho Technologies <cnaranjo@kijho.com>
     * @since 1.0 18/02/2014
     * @param array[string] $index indices del arreglo de parametros
     * @param array[string] $order arreglo de parametros
     * @return array[string url, array[order, image]] elementos para el ordemaniento
     */
    public static function getUrlOrderFromParameters($index, $order, $container = '') 
    {
        $orderBy = array();
        $orderBy['url'] = null;
        foreach($index as $item)
        {
            $orderBy[$item] = array();
            $orderBy[$item]['order'] = null;
            $orderBy[$item]['image'] = null;
            
            if(isset($order[$item]) && $order[$item] != '')
            {
                $orderBy[$item]['order'] = $order[$item] + 1;
                $orderBy['url'] = $orderBy['url'].'&'.$item."=".$order[$item];
                
                if($orderBy[$item]['order'] % 2)
                {
                    $orderBy[$item]['image'] = self::IMAGE_ARROW_UP;
                    
                    if($container)
                    {
                        $imagePath = $container->getParameter('bmak_auctions_scheme').'://'.$container->getParameter('bmak_auctions_host').'/bundles/bmakbackend/images/'.self::IMAGE_ARROW_UP;
                        $orderBy[$item]['htmlImage'] = '<img src="'.$imagePath.'" />';
                    }
                }
                else
                {
                    $orderBy[$item]['image'] = self::IMAGE_ARROW_DOWN;
                    
                    if($container)
                    {
                        $imagePath = $container->getParameter('bmak_auctions_scheme').'://'.$container->getParameter('bmak_auctions_host').'/bundles/bmakbackend/images/'.self::IMAGE_ARROW_DOWN;
                        $orderBy[$item]['htmlImage'] = '<img src="'.$imagePath.'" />';
                    }
                }
            }
            else
            {
                $orderBy[$item]['order'] = 1;
                $orderBy[$item]['image'] = '';
            }
        }
        return $orderBy;
    }    
}