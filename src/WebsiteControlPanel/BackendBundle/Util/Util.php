<?php

namespace WebsiteControlPanel\BackendBundle\Util;

/**
 * Description of Util
 *
 * @author Usuario
 */
class Util {

    public static function getCurrentDate() {
        $timezone = new \DateTimeZone('America/Bogota');
        $datetime = new \DateTime('now');
        $datetime->setTimezone($timezone);
        return $datetime;
    }

    public static function randomPassword($size = 8) {
        $alphabet = "abcdefghijkmnpqrstuwxyzABCDEFGHIJKLMNPQRSTUWXYZ23456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache

        for ($i = 0; $i < $size; $i++) {
            $randPosition = mt_rand(0, $alphaLength);
            $pass[] = $alphabet[$randPosition];
        }

        return implode($pass); //turn the array into a string
    }

}
